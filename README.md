
# Software Freedom Day Australia

Website source code for Software Freedom Day Australia

## Technology
- Python
- Bootstrap
- Piwik

## Local Testing
- PHP: ```cd htdocs\``` then ```php -S 127.0.0.1:8008``` then open ```http://127.0.0.1:8008/index.html```

## Artwork Credits
- Photos: Chris Samuel 2010 (CC BY-SA)
- FSM Designs: Michael Verrenkamp and Ben Minerds (CC0 1.0)
